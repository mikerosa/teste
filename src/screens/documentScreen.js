import * as React from "react";
import { Button, View, Text } from "react-native";

function documentScreen({ navigation }) {
  return (
    <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
      <Text>Documents</Text>
      <Button
        title="Go to Register"
        onPress={() => navigation.navigate("registerScreen")}
      />
    </View>
  );
}

export default documentScreen;
