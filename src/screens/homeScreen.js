
import React, { useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  Button,
  TouchableOpacity,
  FlatList,
} from "react-native";

export default function homeScreen({ navigation }) {
  const data = [

    {
      id: 0,
      title: "Realizar Cadastro",
      subTitle: "30/04/2020 - 17:59",
      counter: 15,
      total: 15,
      isActive: false,
      to: "Register",
    },
    {
      id: 1,
      title: "Documentacao Basica",
      subTitle: "Agardando etapa 1",
      counter: 0,
      total: 0,
      isActive: false,
      to: "Document",
    },
    {
      id: 2,
      title: "Definicao de Tese",
      subTitle: "Agardando etapa 2",
      counter: 0,
      total: 0,
      isActive: false,
      to: "ThesisDefinition",
    },
    {
      id: 3,
      title: "Fluxo de Tese",
      subTitle: "Agardando etapa 3",
      counter: 0,
      total: 0,
      isActive: false,
      to: "ThesisFlow",
    },
    {
      id: 4,
      title: "Status do Benefio",
      subTitle: "Agardando etapa 4",
      counter: 0,
      total: 0,
      isActive: false,
      to: "Benefit",
    },

  ];
  const [listData, setListData] = useState(data);
  return (
    <View>
      <FlatList
        data={listData}
        renderItem={({ item }) => (
          <TouchableOpacity
            style={styles.buttonList}
            onPress={() => navigation.navigate(item.to)}
          >
            <View style={styles.line}>
              <Text style={styles.title}>
                {item.id + 1}.{"  "}
                {item.title}
              </Text>
              <Text style={[styles.total]}>
                {item.counter > 0
                  ? item.counter + "/" + item.total
                  : "A definir"}
                {}
              </Text>
            </View>
            <Text style={styles.subTitle}>Data Limite: {item.subTitle}</Text>
          </TouchableOpacity>
        )}
      ></FlatList>
    </View>
  );
}
const styles = StyleSheet.create({
  container: {
    flex: 1,

  },
  appBar: {
    fontSize: 130,
  },
  content: {
    flexGrow: 9,
    backgroundColor: "#ccc",
  },
  footer: {
    flex: 5,
    backgroundColor: "#3498db",
  },

  subTitle: {
    paddingLeft: 10,
    fontSize: 9,
    color: "#4f4f4f",
  },
  total: {
    color: "#ffd530",
    paddingRight: 10,
    fontWeight: "500",
    position: "absolute",
    left: 300,
  },
  title: {
    color: "#4f4f4f",
    padding: 10,
    fontWeight: "500",
    fontSize: 17,
    justifyContent: "flex-start",
  },
  line: {
    flexDirection: "row",
    alignItems: "center",
  },
  buttonList: {
    padding: 10,
    margin: 10,
    height: 100,
    backgroundColor: "white",
    borderLeftWidth: 8,
    borderRadius: 5,

    borderLeftColor: "#ffd530",
    flexDirection: "column",
    alignItems: "flex-start",
  },
});
