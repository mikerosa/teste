// import React from 'react';
// import { StyleSheet, Text, View } from 'react-native';
// import {  DefaultTheme, Appbar, Provider as PaperProvider } from 'react-native-paper';
// import ButtonList from './src/components/ButtonList'

// const theme = {
//   ...DefaultTheme,
//   roundness: 2,
//   fonts: {
//     ...DefaultTheme.fonts,
//     regular: {
//       fontFamily: 'System',
//       fontWeight: '500',
//       fontSize: 15
//     },
//   },
//   colors: {
//     ...DefaultTheme.colors,
//     primary: '#37d7b2',
//     accent: '#f1c40f',

//   },

// };
// const data = [
//   { id: 0, title: 'Realizar Cadastro', subTitle: '30/04/2020 - 17:59', counter: 15, total: 15, isActive: false },
//   { id: 1, title: 'Documentacao Basica', subTitle: 'Agardando etapa 1', counter: 0, total: 0, isActive: false },
//   { id: 2, title: 'Definicao de Tese', subTitle: 'Agardando etapa 2', counter: 0, total: 0, isActive: false },
//   { id: 3, title: 'Fluxo de Tese', subTitle: 'Agardando etapa 3', counter: 0, total: 0, isActive: false },
//   { id: 4, title: 'Status do Benefio', subTitle: 'Agardando etapa 4', counter: 0, total: 0, isActive: false },
// ]


// export default class App extends React.Component {
//   constructor(props) {
//     super(props);
//     this.state = {
//       list: data
//     };
//     this._handleButton = this._handleButton.bind(this); // setting the THIS
//     this._goBack = this._goBack.bind(this); // setting the THIS
//     this._handleSearch = this._handleSearch.bind(this); // setting the THIS
//     this._handleMore = this._handleMore.bind(this); // setting the THIS
//   }

//   _goBack = () => console.log('Went back');

//   _handleSearch = () => console.log('Searching');

//   _handleMore = () => console.log('Shown more');

//   _handleButton = (id) => {
//     for (let i = 0; i < this.state.list.length; i++) {
//       if (this.state.list[i].id === id) {
//         let aux = this.state.list[id].isActive = true
//         this.setState({ ...this.state.list, aux })
//       } else {
//         let aux = this.state.list[i].isActive = false
//         this.setState({ ...this.state.list, aux })
//       }
//     }
//   };

//   render() {
//     return (
//       <PaperProvider theme={theme} dark={true}>
//         <Appbar.Header dark >
//           <Appbar.Action icon="door-open" onPress={this._goBack} />
//           <Appbar.Content
//             title="Meu Encaminhamento"

//           />
//           <Appbar.Action icon="magnify" onPress={this._handleSearch} />
//           <Appbar.Action icon="dots-vertical" onPress={this._handleMore} />
//         </Appbar.Header>
//         <View style={styles.content}>
//           {data.map(item => <ButtonList item={item} _handleButton={this._handleButton} ></ButtonList>
//           )}
//         </View>
//         <View style={styles.footer}>

//         </View>

//       </PaperProvider >
//     )

//   }
// }



// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     backgroundColor: 'yellow',
//     color: 'white'
//   },
//   appBar: {
//     fontSize: 130
//   },
//   content: {
//     flexGrow: 9,
//     backgroundColor: '#ccc'
//   },
//   footer: {
//     flex: 5,
//     backgroundColor: '#3498db'
//   },

//   subTitle: {

//     paddingLeft: 10,
//     fontSize: 9,
//     color: '#4f4f4f'
//   },
//   total: {
//     color: '#ffd530',
//     paddingRight: 10,
//     fontWeight: '500',
//     position: 'absolute',
//     left: 300

//   },
//   title: {
//     color: '#4f4f4f',
//     padding: 10,
//     fontWeight: '500',
//     fontSize: 17,
//     justifyContent: 'flex-start'


//   },
//   line: {
//     flexDirection: 'row',
//     alignItems: 'center',


//   },
//   buttonList: {
//     padding: 10,
//     margin: 10,
//     height: 100,
//     backgroundColor: 'white',
//     borderLeftWidth: 8,
//     borderRadius: 5,

//     borderLeftColor: '#ffd530',
//     flexDirection: 'column',
//     alignItems: 'flex-start',


//   }

// });
import React from 'react';
import { View } from 'react-native';
import { Checkbox, TextInput, Button, Card, } from 'react-native-paper';


export default class registerScreen extends React.Component {
  state = {
    checked: false,
  };
  render() {
    const { checked } = this.state;
    return (
      <View>

        <Card style={{ marginVertical: 1, marginHorizontal: 20 }}>
          {/* <Divider /> */}
          <Card.Title subtitle="Dados Basicos" />
          <View style={{ flexDirection: 'row' }}>
            <TextInput
              mode='outlined'
              style={{ margin: 10, backgroundColor: 'white',flex:1 }}
              label='Nome'
            />
            <Checkbox status={checked ? 'checked' : 'unchecked'}
              onPress={() => { this.setState({ checked: !checked }); }} />
          </View>
          <TextInput
            mode='outlined'
            style={{ margin: 10 }}
            label='Endereco'
          />
          <TextInput
            mode='outlined'
            style={{ margin: 10 }}
            label='Email'
          />

          <TextInput
            mode='outlined'
            style={{ margin: 10 }}
            label='Nome'
          />
          <Card.Actions>
            <Button>Cancel</Button>
            <Button >Ok</Button>
          </Card.Actions>
        </Card>
      </View>)
  }
}

