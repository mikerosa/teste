import React from 'react'
import { Appbar, Provider as PaperProvider } from 'react-native-paper';


export default function AppBar() {
    return (


        <Appbar.Header dark >
            <Appbar.Action icon="door-open" onPress={() => { }} />
            <Appbar.Content
                title="Meu Encaminhamento"

            />
            <Appbar.Action icon="magnify" onPress={() => { }} />
            <Appbar.Action icon="dots-vertical" onPress={() => { }} />
        </Appbar.Header>

    )
}
