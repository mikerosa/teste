/*Example of Expandable ListView in React Native*/
import React, { Component } from 'react';
//import react in our project
import {
    LayoutAnimation,
    StyleSheet,
    View,
    Text,
    ScrollView,
    UIManager,
    TouchableOpacity,
    Platform,
} from 'react-native';
import { TextInput } from 'react-native-paper';
import SubMenu from './SubMenu';

//import basic react native components

class ExpandableItemComponent extends Component {
    //Custom Component for the Expandable List
    constructor() {
        super();
        this.state = {
            layoutHeight: 0,
        };
    }
    componentWillReceiveProps(nextProps) {
        if (nextProps.item.isExpanded) {
            this.setState(() => {
                return {
                    layoutHeight: null,
                };
            });
        } else {
            this.setState(() => {
                return {
                    layoutHeight: 0,
                };
            });
        }
    }
    shouldComponentUpdate(nextProps, nextState) {
        if (this.state.layoutHeight !== nextState.layoutHeight) {
            return true;
        }
        return false;
    }

    render() {
        return (
            <View>
                {/*Header of the Expandable List Item*/}
                <TouchableOpacity
                    activeOpacity={0.8}
                    onPress={this.props.onClickFunction}
                    style={styles.buttonList}>

                    <View style={styles.line}>
                        <Text style={styles.title}>
                            {this.props.item.id + 1}.{"  "}
                            {this.props.item.title}
                        </Text>
                        <Text style={[styles.total]}>
                            {this.props.item.counter > 0
                                ? this.props.item.counter + "/" + this.props.item.total
                                : "A definir"}

                        </Text>
                    </View>
                    <Text style={styles.subTitle}>Data Limite: {this.props.item.subTitle}</Text>



                </TouchableOpacity>
                <View
                    style={{
                        height: this.state.layoutHeight,
                        overflow: 'hidden',
                    }}>
                    {/*Content under the header of the Expandable List Item*/}
                    {/* {this.props.item.subcategory.map((item, key) => (
                        <TouchableOpacity
                            key={key}
                            style={styles.content}
                            onPress={() => alert('Id: ' + item.id + ' val: ' + item.val)}>
                           
                            <Text style={styles.text}>
                                {key}. {item.val}
                            </Text>
                            <TextInput/>
                            <View style={styles.separator} />
                        </TouchableOpacity>
                    ))} */}
                     <SubMenu id={this.props.item.id}/>
                </View>
            </View>
        );
    }
}

export default class App extends Component {
    //Main View defined under this Class
    constructor() {
        super();
        if (Platform.OS === 'android') {
            UIManager.setLayoutAnimationEnabledExperimental(true);
        }
        this.state = { listDataSource: CONTENT };
    }

    updateLayout = index => {
        LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
        const array = [...this.state.listDataSource];
        //For Single Expand at a time
        array.map((value, placeindex) =>
            placeindex === index
                ? (array[placeindex]['isExpanded'] = !array[placeindex]['isExpanded'])
                : (array[placeindex]['isExpanded'] = false)
        );

        //For Multiple Expand at a time
        //array[index]['isExpanded'] = !array[index]['isExpanded'];
        this.setState(() => {
            return {
                listDataSource: array,
            };
        });
    };

    render() {
        return (
            <View style={styles.container}>

                <ScrollView>
                    {this.state.listDataSource.map((item, key) => (
                        <ExpandableItemComponent
                            key={item.id}
                            onClickFunction={this.updateLayout.bind(this, key)}
                            item={item}
                        />
                    ))}
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    subTitle: {
        paddingLeft: 10,
        fontSize: 9,
        color: "#4f4f4f",
    },
    total: {
        color: "#ffd530",
        paddingRight: 10,
        fontWeight: "500",
        position: "absolute",
        left: 300,
    },
    title: {
        color: "#4f4f4f",
        padding: 10,
        fontWeight: "500",
        fontSize: 17,
        justifyContent: "flex-start",
    },
    line: {
        flexDirection: "row",
        alignItems: "center",
    },
    buttonList: {
        padding: 10,
        marginTop:10,
        // margin: 10,
        marginBottom:0,

        marginHorizontal:10,
        height: 100,
        backgroundColor: "white",
        borderLeftWidth: 8,
        borderRadius: 5,

        borderLeftColor: "#ffd530",
        flexDirection: "column",
        alignItems: "flex-start",
    },
    container: {
        flex: 1,
        paddingTop: 30,
        backgroundColor: '#F5FCFF',
    },
    topHeading: {
        paddingLeft: 10,
        fontSize: 20,
    },
    header: {
        backgroundColor: '#F5FCFF',
        padding: 16,
    },
    headerText: {
        fontSize: 16,
        fontWeight: '500',
    },
    separator: {
        height: 0.5,
        backgroundColor: '#808080',
        width: '95%',
        marginLeft: 16,
        marginRight: 16,
    },
    text: {
        fontSize: 16,
        color: '#606070',
        padding: 10,
    },
    content: {

        paddingLeft: 10,
        paddingRight: 10,
        backgroundColor: '#fff',
    },
});

//Dummy content to show
//You can also use dynamic data by calling webservice
const CONTENT = [
    {
        id: 0,
        isExpanded: false,
        subcategory: [{ id: 1, val: 'Sub Cat 1' }, { id: 3, val: 'Sub Cat 3' }],
        title: "Realizar Cadastro",
        subTitle: "30/04/2020 - 17:59",
        counter: 15,
        total: 15,
    },
    {
        id: 1,
        isExpanded: false,
        subcategory: [{ id: 1, val: 'Sub Cat 1' }, { id: 3, val: 'Sub Cat 3' }],
        title: "Documentacao Basica",
        subTitle: "Agardando etapa 1",
        counter: 0,
        total: 0,
    },
    {
        id: 2,
        isExpanded: false,
        category_name: 'Item 3',
        subcategory: [{ id: 7, val: 'Sub Cat 7' }, { id: 9, val: 'Sub Cat 9' }],
        title: "Definicao de Tese",
        subTitle: "Agardando etapa 2",
        counter: 0,
        total: 0,
    },
    {
        id: 3,
        isExpanded: false,
        category_name: 'Item 2',
        subcategory: [{ id: 4, val: 'Sub Cat 4' }, { id: 5, val: 'Sub Cat 5' }],
        title: "Fluxo de Tese",
        subTitle: "Agardando etapa 3",
        counter: 0,
        total: 0,
    },
    {
        id: 4,
        isExpanded: false,
        subcategory: [{ id: 1, val: 'Sub Cat 1' }, { id: 3, val: 'Sub Cat 3' }],
        title: "Status do Benefio",
        subTitle: "Agardando etapa 4",
        counter: 0,
        total: 0,
    },



];
