import React from "react";
import { TouchableOpacity, StyleSheet, Text, View, Button } from "react-native";

export default function ButtonList({ item, _handleButton }) {
  console.log("buttonList - " + item.isActive + " - " + item.id);

  const color = item.counter > 0 ? { color: "#4f4f4f" } : { color: "#ffd530" };
  const borderLeftColor = item.isActive
    ? { borderLeftColor: "#ffd530" }
    : { borderLeftColor: "white" };
  return (
    <TouchableOpacity
      enabled
      style={[styles.buttonList, borderLeftColor]}
      onPress={() => _handleButton(item.id)}
    >
      <View style={styles.line}>
        <Text style={styles.title}>
          {item.id + 1}.{"  "}
          {item.title}
        </Text>
        <Text style={[styles.total, color]}>
          {item.counter > 0 ? item.counter + "/" + item.total : "A definir"}
          {}
        </Text>
      </View>
      <Text style={styles.subTitle}>Data Limite: {item.subTitle}</Text>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "yellow",
    color: "white",
  },
  appBar: {
    fontSize: 130,
  },
  content: {
    flexGrow: 9,
    backgroundColor: "#ccc",
  },
  footer: {
    flex: 5,
    backgroundColor: "#3498db",
  },

  subTitle: {
    paddingLeft: 10,
    fontSize: 9,
    color: "#4f4f4f",
  },
  total: {
    color: "#ffd530",
    paddingRight: 10,
    fontWeight: "500",
    position: "absolute",
    left: 300,
  },
  title: {
    color: "#4f4f4f",
    padding: 10,
    fontWeight: "500",
    fontSize: 17,
    justifyContent: "flex-start",
  },
  line: {
    flexDirection: "row",
    alignItems: "center",
  },
  buttonList: {
    padding: 10,
    margin: 10,
    height: 100,
    backgroundColor: "white",
    borderLeftWidth: 8,
    borderRadius: 5,

    borderLeftColor: "#ffd530",
    flexDirection: "column",
    alignItems: "flex-start",
  },
});
