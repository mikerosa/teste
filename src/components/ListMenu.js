import * as React from 'react';
import { List, Checkbox } from 'react-native-paper';
import { View, Text, StyleSheet } from 'react-native';

const item = [{
    id: 0,
    title: "Realizar Cadastro",
    subTitle: "30/04/2020 - 17:59",
    counter: 15,
    total: 15,
    isActive: false,
    to: "Register"
}]

class MyComponent extends React.Component {
    state = {
        expanded: true
    }

    _handlePress = () =>
        this.setState({
            expanded: !this.state.expanded
        });

    render() {
        return (
            <List.Section title="Accordions">
                <List.Subheader>My List Title</List.Subheader>
                <List.Accordion
                    title="Uncontrolled Accordion"
                    left={props => <List.Icon {...props} icon="folder" />}
                >
                    
                    <List.Item title="First item" />
                    <List.Item title="Second item" />
                </List.Accordion>

                <List.Accordion
                    title="Controlled Accordion"
                    left={props => <List.Icon {...props} icon="folder" />}
                    expanded={this.state.expanded}
                    onPress={this._handlePress}
                >
                    <List.Item title="First item" />
                    <List.Item title="Second item" />
                </List.Accordion>
            </List.Section>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,

    },
    appBar: {
        fontSize: 130,
    },
    content: {
        flexGrow: 9,
        backgroundColor: "#ccc",
    },
    footer: {
        flex: 5,
        backgroundColor: "#3498db",
    },

    subTitle: {
        paddingLeft: 10,
        fontSize: 9,
        color: "#4f4f4f",
    },
    total: {
        color: "#ffd530",
        paddingRight: 10,
        fontWeight: "500",
        position: "absolute",
        left: 300,
    },
    title: {
        color: "#4f4f4f",
        padding: 10,
        fontWeight: "500",
        fontSize: 17,
        justifyContent: "flex-start",
    },
    line: {
        flexDirection: "row",
        alignItems: "center",
    },
    buttonList: {
        padding: 10,
        margin: 10,
        height: 100,
        backgroundColor: "white",
        borderLeftWidth: 8,
        borderRadius: 5,

        borderLeftColor: "#ffd530",
        flexDirection: "column",
        alignItems: "flex-start",
    },
});


export default MyComponent;