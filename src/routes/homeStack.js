import * as React from 'react';
import { View, Text } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Home from '../screens/homeScreen'
import Register from '../screens/registerScreen'
import Document from '../screens/documentScreen';
import Benefit from '../screens/benefitStatus';
import ThesisDefinition from '../screens/thesisDefinition';
import ThesisFlow from '../screens/thesisFlow';
import { IconButton,Colors } from 'react-native-paper';




const Stack = createStackNavigator();

function LogoTitle() {
    return (
        <View style={{flexDirection:'row',alignItems:'center',height:100}}>
            <IconButton
                icon="door-open"
                color={Colors.white}
                size={35}
                onPress={() => console.log('Pressed')}
            />
            <Text style={{fontSize:26,color:'white'}} >Meu Encaminhamento</Text>
        </View>
    );
}

function homeStack() {
    return (
        <NavigationContainer>
            <Stack.Navigator screenOptions={{

                headerStyle: {
                    backgroundColor: '#37d7b2',
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                    fontWeight: 'bold',
                }
            }}>
                <Stack.Screen name="Home"

                    component={Home}
                    options={{ headerTitle: props => <LogoTitle {...props} />,headerTitleAlign:'left' }}
                />
                <Stack.Screen name="Register" component={Register} />
                <Stack.Screen name="Document" component={Document}  />
                <Stack.Screen name="Benefit" component={Benefit} />
                <Stack.Screen name="ThesisDefinition" component={ThesisDefinition} />
                <Stack.Screen name="ThesisFlow" component={ThesisFlow} />
            </Stack.Navigator>
        </NavigationContainer>
    );
}

export default homeStack;