import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { DefaultTheme, Appbar, Provider as PaperProvider } from 'react-native-paper';

// import ButtonList from './src/components/ButtonList'
// import Navigator from './src/routes/homeStack'
// import ListMenu from './src/components/ListMenu'
import AppBar from './src/components/AppBar'
import List from './src/components/ExpandableList'


const theme = {
  ...DefaultTheme,
  roundness: 2,
  fonts: {
    ...DefaultTheme.fonts,
    regular: {
      fontFamily: 'System',
      fontWeight: '500',
      fontSize: 15
    },
  },
  colors: {
    ...DefaultTheme.colors,
    primary: '#37d7b2',
    accent: '#f1c40f',

  },

};

export default class App extends React.Component {

  render() {
    return (
      <PaperProvider theme={theme} dark={true} >
        <AppBar/>
        <View style={styles.content}>
          {/* <Navigator /> */}
          {/* <ListMenu/> */}
          <List/>
        </View>
        <View style={styles.footer}>
       
        </View>

      </PaperProvider >
    )

  }
}



const styles = StyleSheet.create({


  content: {
    flexGrow: 9,
    backgroundColor: '#ccc'
  },
  footer: {
    flex: 1,
    backgroundColor: '#3498db'
  },

});
